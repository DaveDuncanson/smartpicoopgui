/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;

import org.eclipse.paho.mqttv5.common.MqttMessage;

/**
 * Simple container class that holds an MQTT Topic & Message instance 
 *
 */
public class MQTTUpdate{
	private String 		topic; 
	private MqttMessage message;
	
	
	/**
	 * Constructor that assigns the passed values
	 * @param topic String containing the MQTT topic to be assigned
	 * @param message MqttMessage instance to be assigned 
	 */
	public MQTTUpdate( String topic, MqttMessage message ) {
		this.topic = topic;
		this.message = message;
	}
	
	public String getTopic() {
		return topic;
	}

	public MqttMessage getMessage() {
		return message;
	}
}
