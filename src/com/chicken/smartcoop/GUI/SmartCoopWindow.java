/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;

import com.chicken.smartcoop.mqtt.MQTT;
import com.pi4j.system.NetworkInfo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.layout.FillLayout;

import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttActionListener;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttCallback;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptionsBuilder;
import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.MqttPersistenceException;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;


/**
 * Main class for the SmartCoopWindow Application
 *
 */
public class SmartCoopWindow implements MqttCallback {
	
	private String mqttBroker 		= MQTT.DEFAULT_MQTT_BROKER;
	private String mqttHostname 	= MQTT.DEFAULT_MQTT_HOSTNAME;
	private String mqttClientId 	= "SmartCoopGUIClient";
	private String mqttUsername 	= MQTT.DEFAULT_MQTT_USERNAME;
	private String mqttPassword 	= MQTT.DEFAULT_MQTT_PASSWORD;


	private MqttAsyncClient asyncClient;
	public MqttAsyncClient getAsyncClient() {
		return asyncClient;
	}
	
	public String getMQTTHostname() { return this.mqttHostname; }
	
	private Shell shell;
	private Display display;
	private Tab[] tabs;
	
	public static final Logger logger = LogManager.getLogger(SmartCoopWindow.class);

	/**
	 * Constructor for the SmartCoopWindow
	 * @param mqttBroker String containing the hostname or ip address of the MQTT broker
	 * @param mqttHostname String containing the hostname of the Smartcoop to be monitored
	 */
	SmartCoopWindow( String mqttBroker, String mqttHostname ) {
		this.mqttBroker = mqttBroker; 
		this.mqttHostname = mqttHostname;
	}
	
	/**
	 * Open and display the Main Window.
	 */
	public void open() {
		if ( mqttBroker.length() > 0 ) {
			connectBroker();
		}
		
		display = Display.getDefault();
		Display.setAppName("SmartPICoopGUI");
		Display.setAppVersion("v1.0");
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		// Disconnect the MQTT Broker Connection before exiting
		try {
			logger.info( "Disconnecting MQTT Broker :" + mqttBroker );
			asyncClient.disconnect();
		} catch (MqttException e) {
			logger.error("Error disconnecting from Broker: " + mqttBroker);
			logger.catching(e);
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(700, 500);
		shell.setText("SmartPICoopGUI - A RaspberryPI based Smart Chicken Coop Monitor");
		shell.setLayout( new FillLayout() );
		
		TabFolder tabFolder = new TabFolder(shell, SWT.BORDER);
		Group tabFolderGroup = new Group (tabFolder, SWT.BORDER);
		
		tabs = new Tab[] {
			new TabMainControl(this, display, tabFolder, SWT.BORDER, 0 ),
			new TabSensorStatus(this, display, tabFolder, SWT.BORDER, 1 ),
			new TabSystemStatus(this, display, tabFolder, SWT.BORDER, 2 ),
			new TabPowerChart(this, display, tabFolder, SWT.BORDER, 3 ),
			new TabFeederChart(this, display, tabFolder, SWT.BORDER, 4 )
		};
		for (Tab tab : tabs) {
			tab.createContents(tabFolderGroup);
		}

		// Add an event listener to write the selected tab to stdout
		tabFolder.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
				Tab selectedTab = (Tab) tabFolder.getSelection()[0].getData();
				selectedTab.doSelection();
				System.out.println(tabFolder.getSelection()[0].getText() + " selected");
			}
		});
	}

	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {

		// Establish our command line options
		Option helpOption = Option.builder("h")
				.longOpt("help")
				.required(false)
				.desc("Displays the Command line Help")
				.build();

		Option jreOption = Option.builder("j")
				.longOpt("JRE")
				.required(false)
				.desc("Show JRE parameters")
				.build();
		
		Option mqttOption = Option.builder("mqtt")
				.longOpt("mqtt-broker")
				.required(false)
				.desc("Specify the MQTT Broker Server")
				.hasArg(true)
				.numberOfArgs(1)
				.build();
		
		Option hostnameOption = Option.builder("h")
				.longOpt("hostname")
				.required(false)
				.desc("Specify the hostname of the coop to monitor")
				.hasArg(true)
				.numberOfArgs(1)
				.build();

		Options options = new Options();
		options.addOption(helpOption);
		options.addOption(jreOption);
		options.addOption(mqttOption);
		options.addOption(hostnameOption);

		// Parse the command line options
		CommandLineParser parser = new DefaultParser();
		String argMQTTBroker = "";
		String argMQTTHostname = "";
		
		try {
			CommandLine cmdLine = parser.parse(options, args);
			if (cmdLine.hasOption( helpOption.getLongOpt() )) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("SmartCoopGUI", options);
			} 
			else {
				// Determine if we want to display the JRE Environmental Information
				if ( cmdLine.hasOption( jreOption.getLongOpt() ) ) {
					System.out.println( "JRE Version :" + System.getProperty( "java.runtime.version" ) );
					System.out.println( "JVM Bit size: " + System.getProperty( "sun.arch.data.model" ) );
					
					/* Total number of processors or cores available to the JVM */
					System.out.println("Available processors (cores): " +  Runtime.getRuntime().availableProcessors());

					/* Total amount of free memory available to the JVM */
					System.out.println("Free memory (bytes): " + Runtime.getRuntime().freeMemory());

					/* This will return Long.MAX_VALUE if there is no preset limit */
					long maxMemory = Runtime.getRuntime().maxMemory();
					
					/* Maximum amount of memory the JVM will attempt to use */
					System.out.println("Maximum memory (bytes): " + (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));

					/* Total memory currently in use by the JVM */
					System.out.println("Total memory (bytes): " + Runtime.getRuntime().totalMemory());

					/* Get a list of all file system roots on this system */
					File[] roots = File.listRoots();

					/* For each file system root, print some info */
					for (File root : roots) {
						System.out.println("File system root: " + root.getAbsolutePath());
						System.out.println("Total space (bytes): " + root.getTotalSpace());
						System.out.println("Free space (bytes): " + root.getFreeSpace());
						System.out.println("Usable space (bytes): " + root.getUsableSpace());
					}
				}
				if ( cmdLine.hasOption( mqttOption.getLongOpt() ) ) {
					String[] optionValues = cmdLine.getOptionValues( mqttOption.getLongOpt());
					argMQTTBroker = optionValues[0];
				}
				else {
					argMQTTBroker = MQTT.DEFAULT_MQTT_BROKER;
				}
				if ( cmdLine.hasOption( hostnameOption.getLongOpt() ) ) {
					String[] optionValues = cmdLine.getOptionValues( hostnameOption.getLongOpt());
					argMQTTHostname = optionValues[0];
				}
				else {
					argMQTTHostname = MQTT.DEFAULT_MQTT_HOSTNAME;
				}
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		String appId = "smartCoopGUIAppid";
		boolean alreadyRunning;
		try {
			JUnique.acquireLock(appId);
			alreadyRunning = false;
		} catch (AlreadyLockedException e) {
			alreadyRunning = true;
		}
		
		// Ensure there is one only instance of the application running. 
		if (!alreadyRunning) {
			try {
				SmartCoopWindow window = new SmartCoopWindow( argMQTTBroker, argMQTTHostname );
				window.open();
			} 
			catch (Exception e) {
				logger.catching(e);
			}
		}
		else {
			logger.error( "There is already an instance of the SMartGUICoop application running, exiting now" );
		}
	}

	/**
	 * Establishes a connection to the MQTT Broker
	 */
	public void connectBroker() {
		try {
			MemoryPersistence persistence = new MemoryPersistence();
			
			this.asyncClient = new MqttAsyncClient(mqttBroker, mqttClientId  + "-" + NetworkInfo.getHostname(), persistence);

			// Lets build our Connection Options:
			MqttConnectionOptionsBuilder conOptsBuilder = new MqttConnectionOptionsBuilder();
			MqttConnectionOptions conOpts = conOptsBuilder.serverURI(mqttBroker)
					.cleanStart(true)
					.username(mqttUsername)
					.password(mqttPassword.getBytes())
					.sessionExpiryInterval((long) 120)
					.automaticReconnect(true)
					.topicAliasMaximum(1000).build();
			
			
			conOpts.setReceiveMaximum( Integer.valueOf(MQTT.mqttMaxInFlight) );
			logger.info("MQTT Recieve MAX = " +  conOpts.getReceiveMaximum().toString() );
			
			asyncClient.setCallback(this);
			
			
			// Lets attempt to connect to the broker
			logger.debug("Connecting to broker: " + mqttBroker);
			asyncClient.connect(conOpts, null, new MqttActionListener() {

				@Override
				public void onSuccess(IMqttToken asyncActionToken) {
					logger.info("Connected to MQTT Broker: " + mqttBroker);
					String smartcoopHostnameTopic = MQTT.TOPIC_SMARTPICOOP + "/" + mqttHostname +  MQTT.TOPIC_REGEX_ALL;
					try {
						
						IMqttToken subToken = asyncClient.subscribe( smartcoopHostnameTopic, MQTT.DEFAULT_MQTT_QOS);
						subToken.waitForCompletion();
						logger.info("MQTT Subscribed to [" + smartcoopHostnameTopic + "]");
					} catch (MqttException e) {
						logger.error("Exception Occured whilst MQTT Subscribing to [" + smartcoopHostnameTopic + "] " + e);
						e.printStackTrace();
					}
				}
				@Override
				public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
					logger.error("Failed to Connect: " + exception.getLocalizedMessage());
				}
			});
		} catch (MqttException e) {
			logger.catching(e);
		} catch (IOException e1) {
			logger.catching(e1);
		} catch (InterruptedException e2) {
			logger.catching(e2);
		}
	}

	@Override
	public void disconnected(MqttDisconnectResponse disconnectResponse) {
		logger.info( "MQTT Broker disconnected: " + disconnectResponse.getReasonString() );
	}

	@Override
	public void mqttErrorOccurred(MqttException exception) {
		logger.error("MQTT Error detected :" + exception.getLocalizedMessage() );
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String incomingMessage = new String(message.getPayload());
		logger.trace("Incoming Message: [" + incomingMessage + "], topic:[" + topic + "]");

		// Determine the topic and pass to the tab instances to process
		for (Tab tab : tabs) {
			if ( topic.contains( MQTT.TOPIC_SMARTPICOOP ) ) {
				tab.queueMQTTMessage( topic, message );
			}
		}	
	}

	@Override
	public void deliveryComplete(IMqttToken token) {
		logger.info( "MQTT delivery Complete: " + token.toString() );
	}

	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		logger.info( "MQTT Connection Complete: " + serverURI );
		
		// Request a full update of the system state
		try {
			MqttMessage msg = new MqttMessage( MQTT.MESSAGE_COMMAND_PUBLISH_STATE.getBytes() );
			msg.setQos(MQTT.DEFAULT_MQTT_QOS);
			IMqttToken subToken = asyncClient.publish(MQTT.TOPIC_COMMAND, msg);
			subToken.waitForCompletion( MQTT.DEFAULT_MQTT_PUBLISH_TIMEOUT );

		} catch (MqttPersistenceException e) {
			logger.catching(e);
		} catch (MqttException e1) {
			logger.catching(e1);
		}
	}

	@Override
	public void authPacketArrived(int reasonCode, MqttProperties properties) {
		logger.info( "MQTT Auth Packet Arrived: " + reasonCode );
	}
}
