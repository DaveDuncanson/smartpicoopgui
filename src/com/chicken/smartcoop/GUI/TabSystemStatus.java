/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;

import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;

import com.chicken.smartcoop.mqtt.MQTT;


/**
 * Implements the functionality of the System Tab
 *
 */
public class TabSystemStatus extends Tab {
	private Label labelPlatformText;
	private Label labelBoardText;
	private Label labelARMFreqText;
	private Label labelCPUTempText;
	private Label labelCPUVoltText;

	private Label labelMemoryTotalText;
	private Label labelMemoryUsedText;
	private Label labelMemoryFreeText;
	
	private Label labelOSNameText;
	private Label labelOSVersionText;
	private Label labelFirmwareDateText;
	private Label labelJavaVendorText;
	private Label labelHostnameText;
	private Label labelIpaddressText;
	
	private Label labelDiskPathText;
	private Label labelDiskTotalText;
	private Label labelDiskFreeText;
	private Label labelDiskUseableText;
	
	/**
	 * Constructor for the System Status Tab
	 * @param window Instance of the parent window
	 * @param parent Instance of the parent tab folder
	 * @param style Style to be assigned to the Tab
	 */
	public TabSystemStatus(SmartCoopWindow window, Display display, TabFolder parent, int style, int index ) {
		super(window, display, parent, style, index );
	}

	public void createContents ( Group tabFolderGroup) {		
		tabItem.setText( "System" );
		
		Group controlsGroup = new Group(tabFolder, SWT.BORDER);
		tabItem.setControl(controlsGroup);
		
		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.marginWidth = 5;
		gridLayout.marginHeight = 5;
		gridLayout.marginTop = 5;
		gridLayout.marginBottom = 5;
		controlsGroup.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, true));
	 	controlsGroup.setLayout(gridLayout); 	
	 	
		// Create a group for the Hardware Parameters 
		Group hardwareGroup = new Group(controlsGroup, SWT.NONE);
		GridLayout gridLayoutHardware= new GridLayout(6, true);
		hardwareGroup.setLayout(gridLayoutHardware);
		hardwareGroup.setLayoutData (new GridData (SWT.FILL, SWT.NONE, true, true));
		hardwareGroup.setText ("Hardware");
		
		Label labelPlatform = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelPlatform.setText( "Platform : " );
		labelPlatform.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelPlatform );
		
		labelPlatformText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelPlatformText.setText( Tab.NOT_AVAILABLE_YET );
		labelPlatformText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelBoard = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelBoard.setText( "Board : " );
		labelBoard.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelBoard );
		
		labelBoardText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelBoardText.setText( Tab.NOT_AVAILABLE_YET );
		labelBoardText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelARMFreq = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelARMFreq.setText( "ARM Freq : " );
		labelARMFreq.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelARMFreq );
		
		labelARMFreqText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelARMFreqText.setText( Tab.NOT_AVAILABLE_YET );
		labelARMFreqText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelCPUTemp = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelCPUTemp.setText( "CPU Temp : " );
		labelCPUTemp.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelCPUTemp );
		
		labelCPUTempText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelCPUTempText.setText( Tab.NOT_AVAILABLE_YET );
		labelCPUTempText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelCPUVolt = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelCPUVolt.setText( "CPU Voltage : " );
		labelCPUVolt.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelCPUVolt );
		
		labelCPUVoltText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelCPUVoltText.setText( Tab.NOT_AVAILABLE_YET );
		labelCPUVoltText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelMemoryTotal = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMemoryTotal.setText( "Memory Total : " );
		labelMemoryTotal.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelMemoryTotal );
		
		labelMemoryTotalText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMemoryTotalText.setText( Tab.NOT_AVAILABLE_YET );
		labelMemoryTotalText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelMemoryUsed = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMemoryUsed.setText( "Memory Used : " );
		labelMemoryUsed.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelMemoryUsed );
		
		labelMemoryUsedText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMemoryUsedText.setText( Tab.NOT_AVAILABLE_YET );
		labelMemoryUsedText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelMemoryFree = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMemoryFree.setText( "Memory Free : " );
		labelMemoryFree.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelMemoryFree );
		
		labelMemoryFreeText = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMemoryFreeText.setText( Tab.NOT_AVAILABLE_YET );
		labelMemoryFreeText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelSpacer1 = new Label(hardwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSpacer1.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 3, 1));
		
		// Create a group for the Software Parameters
		Group softwareGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutDoor= new GridLayout(6, true);
		softwareGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		softwareGroup.setLayout(gridLayoutDoor);
		softwareGroup.setText("Software");
		
		Label labelOSName = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelOSName.setText( "O/S Name : " );
		labelOSName.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelOSName );
		
		labelOSNameText = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelOSNameText.setText( Tab.NOT_AVAILABLE_YET );
		labelOSNameText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelOSVersion = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelOSVersion.setText( "O/S Version : " );
		labelOSVersion.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelOSVersion );
		
		labelOSVersionText = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelOSVersionText.setText( Tab.NOT_AVAILABLE_YET );
		labelOSVersionText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelFirmwareDate = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFirmwareDate.setText( "Firmware Date : " );
		labelFirmwareDate.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelFirmwareDate );
		
		labelFirmwareDateText = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFirmwareDateText.setText( Tab.NOT_AVAILABLE_YET );
		labelFirmwareDateText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelJavaVendor = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelJavaVendor.setText( "Java Vendor : " );
		labelJavaVendor.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelJavaVendor );
		
		labelJavaVendorText = new Label(softwareGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelJavaVendorText.setText( Tab.NOT_AVAILABLE_YET );
		labelJavaVendorText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Networking Parameters
		Group networkGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutNetwork= new GridLayout(6, true);
		networkGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		networkGroup.setLayout(gridLayoutNetwork);
		networkGroup.setText("Networking");
		
		Label labelHostname = new Label(networkGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelHostname.setText( "Hostname: " );
		labelHostname.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelHostname );
		
		labelHostnameText = new Label(networkGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelHostnameText.setText( Tab.NOT_AVAILABLE_YET );
		labelHostnameText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelIpaddr = new Label(networkGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelIpaddr.setText( "IP Addresses : " );
		labelIpaddr.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont(labelIpaddr );
		
		labelIpaddressText = new Label(networkGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelIpaddressText.setText( Tab.NOT_AVAILABLE_YET );
		labelIpaddressText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Disk Parameters
		Group diskGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutDisk= new GridLayout(6, true);
		diskGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		diskGroup.setLayout(gridLayoutDisk);
		diskGroup.setText("Disk");

		Label labelDiskPath = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskPath.setText( "Path: " );
		labelDiskPath.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelDiskPath );

		labelDiskPathText = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskPathText.setText( Tab.NOT_AVAILABLE_YET );
		labelDiskPathText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));

		Label labelDiskTotal = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskTotal.setText( "Total : " );
		labelDiskTotal.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont(labelDiskTotal );

		labelDiskTotalText = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskTotalText.setText( Tab.NOT_AVAILABLE_YET );
		labelDiskTotalText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));

		Label labelDiskFree = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskFree.setText( "Free: " );
		labelDiskFree.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelDiskFree );

		labelDiskFreeText = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskFreeText.setText( Tab.NOT_AVAILABLE_YET );
		labelDiskFreeText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));

		Label labelDiskUseable = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskUseable.setText( "Useable : " );
		labelDiskUseable.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont(labelDiskUseable );

		labelDiskUseableText = new Label(diskGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDiskUseableText.setText( Tab.NOT_AVAILABLE_YET );
		labelDiskUseableText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));

		controlsGroup.pack();
	}

	@Override
	public void processMQTTMessage(String topic, MqttMessage message) {		
		String payload = new String( message.getPayload() );
		
		// Replace the host with the token back into the topic string so we can just use a constant string match
		topic = topic.replaceFirst( window.getMQTTHostname(), MQTT.TOKEN_HOSTNAME );
		switch ( topic ) {
		
		case MQTT.TOPIC_SYSTEM_PLATFORM :
			labelPlatformText.setText(payload);
			break;
		
		case MQTT.TOPIC_SYSTEM_BOARD :
			labelBoardText.setText(payload);
			break;
			
		case MQTT.TOPIC_SYSTEM_ARM_FREQ :
			labelARMFreqText.setText(payload);
			break;
			
		case MQTT.TOPIC_SYSTEM_CPU_TEMP :
			labelCPUTempText.setText(payload);
			break;
			
		case MQTT.TOPIC_SYSTEM_CPU_VOLT :
			labelCPUVoltText.setText(payload);		
			break;
	
		case  MQTT.TOPIC_SYSTEM_OS_NAME :
			labelOSNameText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_OS_VERSION :
			labelOSVersionText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_FIRMWARE_DATE :
			labelFirmwareDateText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_MEMORY_TOTAL :
			try {
				Long mem = Long.valueOf(payload);
				float megBytes = (float) mem / 1000000;
				labelMemoryTotalText.setText( String.format ("%.2f", megBytes) + " MBytes");
			} catch ( NumberFormatException e ) {
				labelMemoryTotalText.setText(payload);
				logger.catching(e);
			}
			break;
			
		case  MQTT.TOPIC_SYSTEM_MEMORY_USED :
			try {
				Long mem = Long.valueOf(payload);
				float megBytes = (float) mem / 1000000;
				labelMemoryUsedText.setText( String.format ("%.2f", megBytes) + " MBytes");
			} catch ( NumberFormatException e ) {
				labelMemoryUsedText.setText(payload);
				logger.catching(e);
			}
			break;
			
		case  MQTT.TOPIC_SYSTEM_MEMORY_FREE :
			try {
				Long mem = Long.valueOf(payload);
				float megBytes = (float) mem / 1000000;
				labelMemoryFreeText.setText( String.format ("%.2f", megBytes) + " MBytes");
			} catch ( NumberFormatException e ) {
				labelMemoryFreeText.setText(payload);
				logger.catching(e);
			}
			break;
			
		case  MQTT.TOPIC_SYSTEM_JAVA_VENDOR :
			labelJavaVendorText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_JAVA_VERSION :
			labelOSVersionText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_IPADDRESS :
			labelIpaddressText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_HOSTNAME :
			labelHostnameText.setText(payload);
			break;
		
		case  MQTT.TOPIC_SYSTEM_DISK_PATH :
			labelDiskPathText.setText(payload);
			break;
			
		case  MQTT.TOPIC_SYSTEM_DISK_TOTAL :
			try {
				Long mem = Long.valueOf(payload);
				float megBytes = (float) mem / 1000000000;
				labelDiskTotalText.setText( String.format ("%.2f", megBytes) + " GBytes");
			} catch ( NumberFormatException e ) {
				labelDiskTotalText.setText(payload);
				logger.catching(e);
			}
			break;
			
		case  MQTT.TOPIC_SYSTEM_DISK_FREE :
			try {
				Long mem = Long.valueOf(payload);
				float megBytes = (float) mem / 1000000000;
				labelDiskFreeText.setText( String.format ("%.2f", megBytes) + " GBytes");
			} catch ( NumberFormatException e ) {
				labelDiskFreeText.setText(payload);
				logger.catching(e);
			}
			break;
			
		case  MQTT.TOPIC_SYSTEM_DISK_USEABLE :
			try {
				Long mem = Long.valueOf(payload);
				float megBytes = (float) mem / 1000000000;
				labelDiskUseableText.setText( String.format ("%.2f", megBytes) + " GBytes");
			} catch ( NumberFormatException e ) {
				labelDiskUseableText.setText(payload);
				logger.catching(e);
			}
			break;
		
		default:
			logger.trace( "Tab System Status: Topic: " + topic + " has been ignored [" + payload + "]");
		}
	}

	@Override
	public void doSelection() {
		// TODO Auto-generated method stub
		
	}
}