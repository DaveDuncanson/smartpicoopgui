/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;

import java.util.Queue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import com.chicken.smartcoop.mqtt.MQTT;

/**
 * 
 * @Abstract Class that forms the base class for all of the Tabs in the SmartCoopGUI Window
 *
 */
abstract class Tab implements Runnable {
	
	protected final static int 		DEFAULT_BUTTON_WIDTH = 250;
	protected final static int 		DEFAULT_BUTTON_HEIGHT = 50;
	protected final static String	NOT_AVAILABLE_YET = "Not Available Yet";
	
	
	protected Shell 			shell;
	protected Display 			display;
	protected Group 			tabFolderGroup;
	protected SashForm 			sashForm;
	protected SmartCoopWindow 	window;
	protected TabFolder 		tabFolder;
	protected TabItem			tabItem;
	protected Color				labelStandardFGColour;
	protected Color				labelStandardBGColour;
	
	protected Color				labelHighlightFGColour;
	protected Color				labelHighlightBGColour;
	
	protected Color				labelWarningFGColour;
	protected Color				labelWarningBGColour;
	
	protected Color				labelCriticalFGColour;
	protected Color				labelCriticalBGColour;
	
	protected Color				labelGoodFGColour;
	protected Color				labelGoodBGColour;
	
	protected Queue<MQTTUpdate> mqttMessageQueue;
	protected ReadWriteLock 	queueLock;
	
	public static final Logger logger = LogManager.getLogger(Tab.class);
	
	/**
	 * Constructor for the abstract class
	 * @param window  		Reference to the SWT Window instance
	 * @param display 		Reference to the SWT Display instance
	 * @param tabFolder		Reference to the parent Tab Folder 
	 * @param style			Reference to the SWT Style for the Tab
	 */
	public Tab(SmartCoopWindow window, Display display, TabFolder tabFolder, int style, int index ) {
		this.window = window;
		this.display = display;
		this.tabFolder = tabFolder;
		this.tabItem = new TabItem(tabFolder, SWT.BORDER, index);
		this.tabItem.setData(this);
		
		labelStandardFGColour = new Color(display, 206, 92, 0);	
		
		labelHighlightFGColour = new Color( display, 255,255, 0 );
	
		labelWarningFGColour = new Color(display, 0, 0, 0 ); // Black
		labelWarningBGColour = new Color( display, 255,255, 0 );
		
		labelCriticalFGColour = new Color(display, 255, 255, 255 ); // White
		labelCriticalBGColour = new Color(display, 255, 0, 0); // Red
		
		labelGoodFGColour = new Color(display, 255, 255, 255 ); // White
		labelGoodBGColour = new Color(display, 0, 255, 0); 	// Green

		// Allocate a Semaphore to manage concurrent access to the MQTT Message Queue.  
		queueLock = new ReentrantReadWriteLock();
		
		// Allocate a queue for the readings from each feeder
		mqttMessageQueue = new CircularFifoQueue<MQTTUpdate>( MQTT.mqttMaxInFlight );
	}

	/**
	 * Assigns a bold font for all of the Labels on the Tabs
	 * @param toSet Label to be updated
	 */
	public void assignLabelFont( Label toSet ) {
		FontData[] fD = toSet.getFont().getFontData();
		fD[0].setStyle(SWT.BOLD);
		toSet.setFont( new Font(display,fD[0]));
		toSet.setForeground( labelStandardFGColour );
	}
	
	/**
	 * Assigns a bold font for all of the Highlighted Labels on the Tabs
	 * @param toSet Label to be updated
	 */
	public void assignHighlightFont( Label toSet ) {
		FontData[] fD = toSet.getFont().getFontData();
		fD[0].setStyle(SWT.BOLD);
		toSet.setFont( new Font(display,fD[0]));
		toSet.setForeground( labelHighlightFGColour );
	}
	
	/**
	 * Creates the "control" widget children. Subclasses override this method to augment the standard controls created.
	 */
	public abstract void createContents( Group tabFolderGroup);
	
	/**
	 * Processes the Tab been selected 
	 */
	public abstract void doSelection();
	
	/**
	 * Adds the passed MQTTUpdate to the message queue to be processed
	 * @param toUpdate MQTTUpdate to be added to the queue
	 */
	public void queueMQTTMessage( String topic, MqttMessage message ) {

		MQTTUpdate mqttUpdate = new MQTTUpdate( topic, message );
		queueLock.writeLock().lock();		
		mqttMessageQueue.add( mqttUpdate );
		queueLock.writeLock().unlock();
		display.asyncExec(this); 
	}
		
	/**
	 * Processes the contents of the incoming MQTT Message Queue
	 * 
	 */
	@Override
	public void run() {
		if (tabItem == null || tabItem.isDisposed()) {
			return;
		}
		
		// Lock the queue and then pass each message for processing 
		queueLock.readLock().lock();
		for ( MQTTUpdate mqttUpdate : mqttMessageQueue ) {
			processMQTTMessage( mqttUpdate.getTopic(), mqttUpdate.getMessage() );
		}
		queueLock.readLock().unlock();

		// Clear the contents of the queue
		queueLock.writeLock().lock();
		mqttMessageQueue.clear();
		queueLock.writeLock().unlock();
	}

	/**
	 * Processes State Messages . Subclasses override this method to handle the system messages
	 */
	public abstract void processMQTTMessage( String topic, MqttMessage message );

}
