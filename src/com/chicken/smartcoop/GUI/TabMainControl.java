/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.MqttPersistenceException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.TabFolder;

import com.chicken.smartcoop.mqtt.MQTT;

/**
 * Implements the Manual Control Tab
 */
public class TabMainControl extends Tab {
	
	private Label labelSystemTimeText;
	private Label labelRTCTimeText; 
	private Label labelHostnameText; 
	private Label labelBatteryVoltageText; 
	private Label labelPowerOnText; 
	private Label labelPowerOffText; 
	private Label labelFeeder1;
	private Label labelFeeder2;
	private Label labelMainOpenText;
	private Label labelMainCloseText;
	private Label labelYardOpenText;
	private Label labelYardCloseText;
	private Label labelFrontDoor;
	private Label labelFrontDoorText;
	
	private Label labelWaterTankText;
	
	private ProgressBar progressFeeder1;
	private ProgressBar progressFeeder2;
	
	private Group  mainDoorGroup; 
	private Group  yardDoorGroup; 
	private Button btnMainOpen;
	private Button btnMainClose;
	private Button btnYardOpen;
	private Button btnYardClose;
	
	public static final Logger logger = LogManager.getLogger(TabMainControl.class);
	
	/**
	 * Constructor for the Manual Control Tab
	 * @param window Instance of the parent window
	 * @param parent Instance of the parent tab folder
	 * @param style Style to be assigned to the Tab
	*/
	public TabMainControl(SmartCoopWindow window, Display display, TabFolder parent, int style, int index ) {
		super(window, display, parent, style, index );
	}

	/**
	 * Allocates and initialises the widgets that compose the Manual Tab
	 */
	public void createContents( Group tabFolderGroup) {
	    
		tabItem.setText( "Main Screen" );
		Group controlsGroup = new Group(tabFolder, SWT.BORDER);
		tabItem.setControl(controlsGroup);
		
		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.marginWidth = 5;
		gridLayout.marginHeight = 5;
		gridLayout.marginTop = 5;
		gridLayout.marginBottom = 5;
		controlsGroup.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, true));
	 	controlsGroup.setLayout(gridLayout);
		
		// Create a group for the Time widgets
		Group timeGroup = new Group(controlsGroup, SWT.NONE);
		GridLayout gridLayoutTime= new GridLayout(6, true);
		timeGroup.setLayout(gridLayoutTime);
		timeGroup.setLayoutData (new GridData (SWT.FILL, SWT.NONE, true, true));
		timeGroup.setText ("Date/Time");
		
		Label labelSystemTime = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSystemTime.setText( "System : " );
		labelSystemTime.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelSystemTime );
		
		// Assign the system colours - yes there must be a better way to do this
		labelStandardFGColour = labelSystemTime.getForeground(); 
		labelStandardBGColour = labelSystemTime.getBackground();
		
		labelSystemTimeText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSystemTimeText.setText( Tab.NOT_AVAILABLE_YET );
		labelSystemTimeText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelHostname = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelHostname.setText( "Monitoring : " );
		labelHostname.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelHostname );
		
		labelHostnameText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelHostnameText.setText( Tab.NOT_AVAILABLE_YET );
		labelHostnameText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelRTCTime = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelRTCTime.setText( "RTC : " );
		labelRTCTime.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelRTCTime );
		
		labelRTCTimeText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelRTCTimeText.setText( Tab.NOT_AVAILABLE_YET );
		labelRTCTimeText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelBatteryVolts = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelBatteryVolts.setText("Battery : ");
		labelBatteryVolts.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelBatteryVolts );
		
		labelBatteryVoltageText= new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelBatteryVoltageText.setText( Tab.NOT_AVAILABLE_YET );
		labelBatteryVoltageText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		labelFeeder1 = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFeeder1.setText( "Feeder 1 : " );
		labelFeeder1.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelFeeder1 );		
		
		// Feeder 1 Progress Bar
		GridData progressData = new GridData();
		progressData.horizontalAlignment = GridData.FILL;
		progressData.verticalAlignment = GridData.CENTER;
		progressData.horizontalSpan = 2;
		
		progressFeeder1 = new ProgressBar( timeGroup, SWT.HORIZONTAL );
		progressData.grabExcessHorizontalSpace = true;	
		progressFeeder1.setLayoutData(progressData);
		progressFeeder1.setMaximum(100);
		progressFeeder1.setMinimum(0);
	
		// Feeder 2 Progress Bar
		labelFeeder2 = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFeeder2.setText( "Feeder 2 : " );
		labelFeeder2.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelFeeder2 );
		
		progressFeeder2 = new ProgressBar( timeGroup, SWT.HORIZONTAL );
		progressFeeder2.setLayoutData( progressData );
		progressFeeder2.setMaximum(100);
		progressFeeder2.setMinimum(0);
		
		Label labelPowerOn = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelPowerOn.setText( "Power On : " );
		labelPowerOn.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelPowerOn );
		
		labelPowerOnText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelPowerOnText.setText( Tab.NOT_AVAILABLE_YET );
		labelPowerOnText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelPowerOff = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelPowerOff.setText( "Power Off : " );
		labelPowerOff.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelPowerOff );
		
		labelPowerOffText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelPowerOffText.setText( Tab.NOT_AVAILABLE_YET );
		labelPowerOffText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelMainOpen = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMainOpen.setText( "Main Open: " );
		labelMainOpen.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelMainOpen );
		
		labelMainOpenText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMainOpenText.setText( Tab.NOT_AVAILABLE_YET );
		labelMainOpenText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelMainClose = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMainClose.setText( "Main Close: " );
		labelMainClose.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelMainClose );
		
		labelMainCloseText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMainCloseText.setText( Tab.NOT_AVAILABLE_YET );
		labelMainCloseText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelYardOpen = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardOpen.setText( "Yard Open: " );
		labelYardOpen.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelYardOpen );
		
		labelYardOpenText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardOpenText.setText( Tab.NOT_AVAILABLE_YET );
		labelYardOpenText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelYardClose = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardClose.setText( "Yard Close: " );
		labelYardClose.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelYardClose );
		
		labelYardCloseText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardCloseText.setText( Tab.NOT_AVAILABLE_YET );
		labelYardCloseText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		labelFrontDoor = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFrontDoor.setText( "Front Door: " );
		labelFrontDoor.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelFrontDoor );
		
		labelFrontDoorText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFrontDoorText.setText( Tab.NOT_AVAILABLE_YET );
		labelFrontDoorText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelWaterTank = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelWaterTank.setText( "Water Tank: " );
		labelWaterTank.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelWaterTank );
		
		labelWaterTankText = new Label(timeGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelWaterTankText.setText( Tab.NOT_AVAILABLE_YET );
		labelWaterTankText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Main Door controls
		mainDoorGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutDoor= new GridLayout(2, true);
		mainDoorGroup.setLayoutData(new GridData (SWT.FILL, SWT.FILL, true, true, 1, 1));
		mainDoorGroup.setLayout(gridLayoutDoor);
		mainDoorGroup.setText("Main Door");
		
		// Create the Main Door controls
		btnMainOpen = new Button(mainDoorGroup, SWT.PUSH);
		btnMainOpen.setText ("Open Door");		
		btnMainOpen.setLayoutData(new GridData (SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Add the Event Handler for the Main Door Open Button
		btnMainOpen.addListener( SWT.Selection, new Listener() {
			public void handleEvent( Event arg0 ) {
				logger.info("Button - Main Gate Open Pressed");
				MqttMessage msg = new MqttMessage( MQTT.MESSAGE_COMMAND_DOOR_MAIN_OPEN.getBytes() );
				msg.setQos(MQTT.DEFAULT_MQTT_QOS);
				try { 
					window.getAsyncClient().publish(MQTT.TOPIC_COMMAND.replaceFirst( MQTT.TOKEN_HOSTNAME, window.getMQTTHostname()) , msg);
					btnMainOpen.setEnabled(false);
				} catch (MqttPersistenceException e) {
					logger.catching(e);
				} catch (MqttException e1) {
					logger.catching(e1);
				}
			}
		});
		
		btnMainClose = new Button(mainDoorGroup, SWT.PUSH);
		btnMainClose.setText ("Close Door");
		btnMainClose.setLayoutData(new GridData (SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Add the Event Handler for the Main Door Close Button
		btnMainClose.addListener( SWT.Selection, new Listener() {
			public void handleEvent( Event arg0 ) {
				logger.info("Button - Main Gate Close Pressed");
				MqttMessage msg = new MqttMessage( MQTT.MESSAGE_COMMAND_DOOR_MAIN_CLOSE.getBytes() );
				msg.setQos(MQTT.DEFAULT_MQTT_QOS);
				try {
					window.getAsyncClient().publish(MQTT.TOPIC_COMMAND.replaceFirst( MQTT.TOKEN_HOSTNAME, window.getMQTTHostname()) , msg);
					btnMainClose.setEnabled(false);
				} catch (MqttPersistenceException e) {
					logger.catching(e);
				} catch (MqttException e1) {
					logger.catching(e1);
				}
			}
		});
		
		// Create a group for the Yard Door controls
		yardDoorGroup = new Group(controlsGroup, SWT.NONE);
		yardDoorGroup.setLayoutData(new GridData (SWT.FILL, SWT.FILL, true, true, 1, 1));
		yardDoorGroup.setLayout(gridLayoutDoor);
		yardDoorGroup.setText("Yard Door");
		
		// Create the Yard Door controls
		btnYardOpen = new Button(yardDoorGroup, SWT.PUSH);
		btnYardOpen.setText ("Open Door");
		btnYardOpen.setLayoutData(new GridData (SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Add the Event Handler for the Yard Door Open Button
		btnYardOpen.addListener( SWT.Selection, new Listener() {
			public void handleEvent( Event arg0 ) {
				logger.info("Button - Yard Gate Open Pressed");
				MqttMessage msg = new MqttMessage( MQTT.MESSAGE_COMMAND_DOOR_YARD_OPEN.getBytes() );
				msg.setQos(MQTT.DEFAULT_MQTT_QOS);
				try {
					window.getAsyncClient().publish(MQTT.TOPIC_COMMAND.replaceFirst( MQTT.TOKEN_HOSTNAME, window.getMQTTHostname()) , msg);
					btnYardOpen.setEnabled(false);
				} catch (MqttPersistenceException e) {
					logger.catching(e);
				} catch (MqttException e1) {
					logger.catching(e1);
				}
			}
		});

		btnYardClose = new Button(yardDoorGroup, SWT.PUSH);
		btnYardClose.setText ("Close Door");		
		btnYardClose.setLayoutData(new GridData (SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		// Add the Event Handler for the Yard Door Close Button
		btnYardClose.addListener( SWT.Selection, new Listener() {
			public void handleEvent( Event arg0 ) {
				logger.info("Button - Yard Gate Close Pressed");
				MqttMessage msg = new MqttMessage( MQTT.MESSAGE_COMMAND_DOOR_YARD_CLOSE.getBytes() );
				msg.setQos(MQTT.DEFAULT_MQTT_QOS);
				try {
					window.getAsyncClient().publish(MQTT.TOPIC_COMMAND.replaceFirst( MQTT.TOKEN_HOSTNAME, window.getMQTTHostname()) , msg);
					btnYardClose.setEnabled(false);
				} catch (MqttPersistenceException e) {
					logger.catching(e);
				} catch (MqttException e1) {
					logger.catching(e1);
				}
			}
		});
		controlsGroup.pack();
	}

	/**
	 * Processes the contents of System Messages
	 */
	@Override
	public void processMQTTMessage(String topic, MqttMessage message) {
		String gateState = GateState.class.getSimpleName() + ":";
		String timeSource = TimeSource.class.getSimpleName() + ":";
		
		String payload = new String( message.getPayload() );
		
		logger.trace("Processing Message: [" + message + "], topic:[" + topic + "]");
		
		// Replace the host with the token back into the topic string so we can just use a constant string match
		topic = topic.replaceFirst( window.getMQTTHostname(), MQTT.TOKEN_HOSTNAME );
		
		switch ( topic ) {
		case MQTT.TOPIC_SYSTEM_TIME :
			labelSystemTimeText.setText( payload );	
			break;
			
		case MQTT.TOPIC_SYSTEM_HOSTNAME :
			labelHostnameText.setText( payload );
			break;
			
		case MQTT.TOPIC_SYSTEM_RTC_TIME :
			labelRTCTimeText.setText( payload );
			break;
			
		case MQTT.TOPIC_SYSTEM_POWER_ON :
			labelPowerOnText.setText( payload );
			break;
			
		case MQTT.TOPIC_SYSTEM_POWER_OFF :
			labelPowerOffText.setText( payload );
			break;
		
		case MQTT.TOPIC_STATE_DOOR_MAIN_DAWN :
			labelMainOpenText.setText( payload );
			break;
		
		case MQTT.TOPIC_STATE_DOOR_MAIN_DUSK :
			labelMainCloseText.setText( payload );
			break;
			
		case MQTT.TOPIC_STATE_DOOR_YARD_OPENING :
			labelYardOpenText.setText( payload );
			break;
		
		case MQTT.TOPIC_STATE_DOOR_YARD_CLOSING :
			labelYardCloseText.setText( payload );
			break;
			
		case MQTT.TOPIC_STATE_BATTERY_VOLTAGE :
			labelBatteryVoltageText.setText( payload );
			break;
	
		case MQTT.TOPIC_STATE_FEEDER_1_LEVEL :
			if ( payload == "DISABLED") {
				progressFeeder2.setEnabled(false);
			}
			else {
				int level = Integer.valueOf(payload);
				assignFeederColour( level, labelFeeder1 );
				progressFeeder1.setSelection(level);
			}
			break;
		
		case MQTT.TOPIC_STATE_FEEDER_2_LEVEL :
			if ( payload.equalsIgnoreCase("DISABLED") ) {
				progressFeeder2.setEnabled(false);
			}
			else {
				int level = Integer.valueOf(payload);
				assignFeederColour( level, labelFeeder2 );
				progressFeeder2.setSelection(level);
			}
			break;
			
		case MQTT.TOPIC_SYSTEM_TIME_SOURCE :
			if ( payload.equalsIgnoreCase( timeSource + TimeSource.SYSTEM.toString() )) {
				assignHighlightFont( labelSystemTimeText );
			}
			break;
			
		case MQTT.TOPIC_STATE_DOOR_FRONT_STATE :
			labelFrontDoorText.setText( payload );
			if ( payload.equals("Open") ) {
				labelFrontDoor.setBackground( labelCriticalBGColour );
				labelFrontDoor.setForeground( labelCriticalFGColour ); 
			}
			else {
				labelFrontDoor.setBackground( labelStandardBGColour);
				labelFrontDoor.setForeground( labelStandardFGColour ); 
			}
			break;
			
		case MQTT.TOPIC_STATE_WATER_TANK_LEVEL :
			labelWaterTankText.setText( payload );
			break;
			
		case MQTT.TOPIC_STATE_DOOR_YARD_STATE :
			
			if ( payload.equalsIgnoreCase( gateState + GateState.DISABLED.toString() )) {
				btnYardOpen.setEnabled(false);
				btnYardClose.setEnabled(false);
				yardDoorGroup.setText("Yard Door [DISABLED]");
			}
			else if ( payload.equalsIgnoreCase( gateState + GateState.OPEN.toString() )) {
				btnYardOpen.setEnabled(false);
				btnYardClose.setEnabled(true);
				yardDoorGroup.setText("Yard Door [OPEN]");
			}
			else if ( payload.equalsIgnoreCase( gateState + GateState.OPENING.toString() )) {
				btnYardOpen.setEnabled(false);
				btnYardClose.setEnabled(true);
				yardDoorGroup.setText("Yard Door [OPENING]");
			}
			else if ( payload.equalsIgnoreCase( gateState + GateState.CLOSED.toString() )) {
				btnYardOpen.setEnabled(true);
				btnYardClose.setEnabled(false);
				yardDoorGroup.setText("Yard Door [CLOSED]");
			}else if ( payload.equalsIgnoreCase( gateState + GateState.CLOSING.toString() )) {
				btnYardOpen.setEnabled(true);
				btnYardClose.setEnabled(false);
				yardDoorGroup.setText("Yard Door [CLOSING]");
			}
			break;
			
		case MQTT.TOPIC_STATE_DOOR_MAIN_STATE :
			if ( payload.equalsIgnoreCase( gateState + GateState.DISABLED.toString() )) {
				btnMainOpen.setEnabled(false);
				btnMainClose.setEnabled(false);
				mainDoorGroup.setText("Main Door [DISABLED]");
			}
			else if ( payload.equalsIgnoreCase( gateState + GateState.OPEN.toString() )) {
				btnMainOpen.setEnabled(false);
				btnMainClose.setEnabled(true);
				mainDoorGroup.setText("Main Door [OPEN]");
			}
			else if ( payload.equalsIgnoreCase( gateState + GateState.OPENING.toString() )) {
				btnMainOpen.setEnabled(false);
				btnMainClose.setEnabled(true);
				mainDoorGroup.setText("Main Door [OPENING]");
			}
			else if ( payload.equalsIgnoreCase( gateState + GateState.CLOSED.toString() )) {
				btnMainOpen.setEnabled(true);
				btnMainClose.setEnabled(false);
				mainDoorGroup.setText("Main Door [CLOSED]");
			}else if ( payload.equalsIgnoreCase( gateState + GateState.CLOSING.toString() )) {
				btnMainOpen.setEnabled(true);
				btnMainClose.setEnabled(false);
				mainDoorGroup.setText("Main Door [CLOSING]");
			}
			break;
			
		default:
			logger.trace( "Tab Main Control: Topic: " + topic + " has been ignored [" + payload + "]");
		}
	}

	/**
	 * Assigns the background colour for the pass Label based on the passed feeder level
	 * @param level integer containing the current feed level (percentage)
	 * @param toAssign Label instance that is to have its background colour assigned
	 */
	private void assignFeederColour( int level, Label toAssign ) {
		if ( level > 30) {			
			toAssign.setBackground( labelStandardBGColour); 
			toAssign.setForeground( labelStandardFGColour ); 
		}
		else if ( level > 15) {
			// Set Warning indicators
			toAssign.setBackground(labelWarningBGColour); 
			toAssign.setForeground(labelWarningFGColour);
		}
		else {
			// Set Critical indicators
			toAssign.setBackground( labelCriticalBGColour );
			toAssign.setForeground( labelCriticalFGColour ); 
		}
	}

	@Override
	public void doSelection() {		
	}
}
