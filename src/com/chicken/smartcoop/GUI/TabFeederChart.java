/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

/**
 * Implements all of the functionality of the feeder chart tab
 */
public class TabFeederChart extends Tab {

	private Browser	browser;
	private final String CHART_URL = "http://192.168.1.205:8080/feeder1/charts";
	
	// Added a comment
	public static final Logger logger = LogManager.getLogger(TabFeederChart.class);
	
	/**
	 * Constructor for the Feeder Tab
	 * @param window Instance of the parent window
	 * @param parent Instance of the parent tab folder
	 * @param style Style to be assigned to the Tab
	 */
	public TabFeederChart(SmartCoopWindow window, Display display, TabFolder parent, int style, int index ) {
		super(window, display, parent, style, index );
	}

	public void createContents(Group tabFolderGroup) {
		tabItem.setText( "Feeder Charts" );
	
		Group controlsGroup = new Group(tabFolder, SWT.BORDER);
		tabItem.setControl(controlsGroup);
		
		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.marginWidth = 5;
		gridLayout.marginHeight = 5;
		gridLayout.marginTop = 5;
		gridLayout.marginBottom = 5;
		controlsGroup.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, true));
	 	controlsGroup.setLayout(gridLayout);

		browser = new Browser(controlsGroup, SWT.NONE);
		GridData layoutData = new GridData(GridData.FILL_BOTH);
		layoutData.horizontalSpan = 1;
		layoutData.verticalSpan = 1;
		browser.setLayoutData(layoutData);
		browser.setUrl(CHART_URL);
		browser.setJavascriptEnabled(true);
		controlsGroup.pack();
	}
	

	@Override
	public void processMQTTMessage(String topic, MqttMessage message) {
		// TODO Auto-generated method stub
	}

	@Override
	public void doSelection() {
		logger.info("Setting URL on selection: " + CHART_URL);
		browser.setUrl(CHART_URL);
	}
}
