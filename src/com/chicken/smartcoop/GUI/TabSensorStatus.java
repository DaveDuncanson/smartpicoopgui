/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/	
package com.chicken.smartcoop.GUI;

import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;

import com.chicken.smartcoop.mqtt.MQTT;


public class TabSensorStatus extends Tab {
	private Label labelMainOpenText;
	private Label labelMainCloseText;
	private Label labelYardOpenText;
	private Label labelYardCloseText;
	private Label labelSolarValueText;
	private Label labelSolarAMPSText;
	private Label labelSolarVoltsText;
	private Label labelSolarPercentText;
	private Label labelFeeder1Text;
	private Label labelFeeder2Text;
	private Label labelWaterTankText;
	private Label labelWaterTroughText;
	private Label labelLightPercentText;
	private Label labelLightLUXText;
	
	/**
	 * Constructor for the System Status Tab
	 * @param window Instance of the parent window
	 * @param parent Instance of the parent tab folder
	 * @param style Style to be assigned to the Tab
	 */
	public TabSensorStatus(SmartCoopWindow window, Display display, TabFolder parent, int style, int index ) {
		super(window, display, parent, style, index );
	}

	public void createContents ( Group tabFolderGroup) {		
		tabItem.setText( "Sensors" );
		
		Group controlsGroup = new Group(tabFolder, SWT.BORDER);
		tabItem.setControl(controlsGroup);
		
		GridLayout gridLayout = new GridLayout(1, true);
		gridLayout.marginWidth = 5;
		gridLayout.marginHeight = 5;
		gridLayout.marginTop = 5;
		gridLayout.marginBottom = 5;
		controlsGroup.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, true));
	 	controlsGroup.setLayout(gridLayout);
		
		// Create a group for the Door Sensors
		Group doorSenorGroup = new Group(controlsGroup, SWT.NONE);
		GridLayout gridLayoutTime= new GridLayout(6, true);
		doorSenorGroup.setLayout(gridLayoutTime);
		doorSenorGroup.setLayoutData (new GridData (SWT.FILL, SWT.NONE, true, true));
		doorSenorGroup.setText ("Door Sensors");
		
		Label labelDoorOpen = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDoorOpen.setText( "Main Open : " );
		labelDoorOpen.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelDoorOpen );
		
		labelMainOpenText = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMainOpenText.setText( Tab.NOT_AVAILABLE_YET );
		labelMainOpenText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelDoorClose = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelDoorClose.setText( "Main Close : " );
		labelDoorClose.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelDoorClose );
		
		labelMainCloseText = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelMainCloseText.setText( Tab.NOT_AVAILABLE_YET );
		labelMainCloseText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelYardDoorOpen = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardDoorOpen.setText( "Yard Open : " );
		labelYardDoorOpen.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelYardDoorOpen );
		
		labelYardOpenText = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardOpenText.setText( Tab.NOT_AVAILABLE_YET );
		labelYardOpenText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelYardDoorClose = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardDoorClose.setText( "Yard Close : " );
		labelYardDoorClose.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelYardDoorClose );
		
		labelYardCloseText = new Label(doorSenorGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelYardCloseText.setText( Tab.NOT_AVAILABLE_YET );
		labelYardCloseText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Power Supply Sensors
		Group powerGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutDoor= new GridLayout(6, true);
		powerGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		powerGroup.setLayout(gridLayoutDoor);
		powerGroup.setText("Power Sensors");
		
		Label labelSolarValue = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarValue.setText( "Solar Value : " );
		labelSolarValue.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelSolarValue );
		
		labelSolarValueText = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarValueText.setText( Tab.NOT_AVAILABLE_YET );
		labelSolarValueText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelSolarAMPS = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarAMPS.setText( "Solar AMPS : " );
		labelSolarAMPS.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelSolarAMPS );
		
		labelSolarAMPSText = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarAMPSText.setText( Tab.NOT_AVAILABLE_YET );
		labelSolarAMPSText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelSolarVolts = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarVolts.setText( "Solar Volts : " );
		labelSolarVolts.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelSolarVolts );
		
		labelSolarVoltsText = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarVoltsText.setText( Tab.NOT_AVAILABLE_YET );
		labelSolarVoltsText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelSolarPercent = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarPercent.setText( "Solar % : " );
		labelSolarPercent.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelSolarPercent );
		
		labelSolarPercentText = new Label(powerGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelSolarPercentText.setText( Tab.NOT_AVAILABLE_YET );
		labelSolarPercentText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Feeder Sensors
		Group feederGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutFeeder= new GridLayout(6, true);
		feederGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		feederGroup.setLayout(gridLayoutFeeder);
		feederGroup.setText("Feeder Sensors");
		
		Label labelFeeder1 = new Label(feederGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFeeder1.setText( "Feeder 1 : " );
		labelFeeder1.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelFeeder1 );
		
		labelFeeder1Text = new Label(feederGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFeeder1Text.setText( Tab.NOT_AVAILABLE_YET );
		labelFeeder1Text.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelFeeder2 = new Label(feederGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFeeder2.setText( "Feeder 2 : " );
		labelFeeder2.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont(labelFeeder2 );
		
		labelFeeder2Text = new Label(feederGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelFeeder2Text.setText( Tab.NOT_AVAILABLE_YET );
		labelFeeder2Text.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Water Sensors
		Group waterGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutWater= new GridLayout(6, true);
		waterGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		waterGroup.setLayout(gridLayoutWater);
		waterGroup.setText("Water Sensors");
				
		Label labelWaterTank = new Label(waterGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelWaterTank.setText( "Water Tank : " );
		labelWaterTank.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelWaterTank );
				
		labelWaterTankText = new Label(waterGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelWaterTankText.setText( Tab.NOT_AVAILABLE_YET );
		labelWaterTankText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
				
		Label labelWaterTrough = new Label(waterGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelWaterTrough.setText( "Water Trough : " );
		labelWaterTrough.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont(labelWaterTrough );
				
		labelWaterTroughText = new Label(waterGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelWaterTroughText.setText( Tab.NOT_AVAILABLE_YET );
		labelWaterTroughText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		// Create a group for the Light Sensor
		Group lightGroup = new Group(controlsGroup, SWT.FILL);
		GridLayout gridLayoutLight= new GridLayout(6, true);
		lightGroup.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		lightGroup.setLayout(gridLayoutLight);
		lightGroup.setText("Light Sensor");
		
		Label labelLightPercent = new Label(lightGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelLightPercent.setText( "Light % : " );
		labelLightPercent.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont( labelLightPercent );
		
		labelLightPercentText = new Label(lightGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelLightPercentText.setText( Tab.NOT_AVAILABLE_YET );
		labelLightPercentText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		Label labelLightLUX = new Label(lightGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelLightLUX.setText( "LUX : " );
		labelLightLUX.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 1, 1));
		assignLabelFont(labelLightLUX );
		
		labelLightLUXText = new Label(lightGroup, SWT.SHADOW_IN | SWT.BORDER);
		labelLightLUXText.setText( Tab.NOT_AVAILABLE_YET );
		labelLightLUXText.setLayoutData(new GridData (SWT.FILL, SWT.NONE, true, true, 2, 1));
		
		controlsGroup.pack();
	}

	/**
	 * Processes the MQTT Messages for the Sensor Tab
	 */
	@Override
	public void processMQTTMessage(String topic, MqttMessage message) {		
		String payload = new String( message.getPayload() );
		
		// Replace the host with the token back into the topic string so we can just use a constant string match
		topic = topic.replaceFirst( window.getMQTTHostname(), MQTT.TOKEN_HOSTNAME );
		
		switch ( topic ) {
		case MQTT.TOPIC_STATE_DOOR_MAIN_OPEN_SENSOR :
			labelMainOpenText.setText( payload );	
			break;
			
		case MQTT.TOPIC_STATE_DOOR_MAIN_CLOSE_SENSOR :
			labelMainCloseText.setText( payload );	
			break;
			
		case MQTT.TOPIC_STATE_DOOR_YARD_OPEN_SENSOR :
			labelYardOpenText.setText( payload );	
			break;
			
		case MQTT.TOPIC_STATE_DOOR_YARD_CLOSE_SENSOR :
			labelYardCloseText.setText( payload );	
			break;
			
		case MQTT.TOPIC_STATE_SOLAR_VALUE :
			labelSolarValueText.setText(payload);
			break;
			
		case MQTT.TOPIC_STATE_SOLAR_VOLTAGE :
			labelSolarVoltsText.setText(payload);
			break;
			
		case MQTT.TOPIC_STATE_SOLAR_AMPS :
			labelSolarAMPSText.setText(payload);
			break;
			
		case MQTT.TOPIC_STATE_SOLAR_PERCENT :
			labelSolarPercentText.setText(payload);
			break;
			
		case MQTT.TOPIC_STATE_FEEDER_1_LEVEL :
			if ( payload.equalsIgnoreCase("DISABLED") ) {
				labelFeeder1Text.setText(payload);
			}
			else {
				labelFeeder1Text.setText(payload + "%");
			}
			break;
			
		case MQTT.TOPIC_STATE_FEEDER_2_LEVEL :
			if ( payload.equalsIgnoreCase("DISABLED") ) {
				labelFeeder2Text.setText(payload);
			}
			else {
				labelFeeder2Text.setText(payload + "%");
			}
			break;
			
		case MQTT.TOPIC_STATE_WATER_TANK_SENSOR :
			labelWaterTankText.setText(payload);
			break;
			
		case MQTT.TOPIC_STATE_WATER_TROUGH_SENSOR :
			labelWaterTroughText.setText(payload);
			break;
			
		case MQTT.TOPIC_STATE_LIGHT_LEVEL :
			labelLightPercentText.setText(payload + "%");
			break;
			
		case MQTT.TOPIC_STATE_LIGHT_LUX :
			labelLightLUXText.setText(payload + " lx");
			break;
			
		default:
			logger.trace( "Tab Sensor Status: Topic: " + topic + " has been ignored [" + payload + "]");
		}
	}

	@Override
	public void doSelection() {		
	}
}